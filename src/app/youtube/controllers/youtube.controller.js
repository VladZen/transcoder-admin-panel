'use strict';
/*globals btoa, atob */

angular.module('transcoder')
	.controller('YoutubeCtrl', ['$scope', '$q', 'CacheService', 'CONFIG', 'ModalAlert', '$timeout', '$rootScope', 'YoutubeService', function($scope, $q, CacheService, CONFIG, ModalAlert, $timeout, $rootScope, YoutubeService) {

		$scope.defaultParams = {
			skip: 0,
			limit: CONFIG.PAGINATION_LIMIT
		};

		$scope.isEdit = false;

		$scope.processing = false;

		var lastParams;

		var base64 = function (string) {
			try {
				return btoa(string);
			} catch(e) {
				ModalAlert.show({
					title: 'Error',
					message: 'Invalid language!'
				});
				$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
				$scope.processing = false;
			}
		};

		var getListProcesser = function(res, params, cacheID){
			if(cacheID){
				CacheService.put(cacheID, res);
			}
			$scope.accounts = res.data.records;
			$scope.pagination = {
				skip: params.skip,
				limit: params.limit,
				total: res.data.total
			};

			lastParams = params;
			$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
		};

		$scope.getList = function(params){
			if(!$scope.processing){
				var cacheID, data, deferred;
				cacheID = 'YoutubeList' + JSON.stringify(params);
				deferred = $q.defer();
				data = CacheService.get(cacheID);
				$scope.processing = true;
				$rootScope.$broadcast(CONFIG.EVENTS.showLoading);
				if (data != null) {
					$timeout(function(){
						getListProcesser (data, params, $scope.accounts);
						$scope.processing = false;
					}, 100);
				} else {
					YoutubeService.list(params).then(function (res) {
						getListProcesser (res, params, $scope.accounts, cacheID);
						$scope.processing = false;
						$rootScope.$broadcast(CONFIG.EVENTS.showContent);
					});
				}
			}
		};

		$scope.edit = function(youtube){
			if(!$scope.processing){
				$scope.processing = true;
				$rootScope.$broadcast(CONFIG.EVENTS.showLoading);
				var cred = base64(JSON.stringify(youtube.credentials));
				if(cred){
					var params = {
						credentials: cred,
						username: youtube.username
					};
					var task = $scope.isEdit ? 'update' : 'new';
					YoutubeService[task + 'Record'](params).then(function(){
						$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
						$scope.processing = false;
						if(task === 'update'){
							$scope.isEdit = false;
						}
						$scope.getList(lastParams);
						ModalAlert.show({
							title: 'Success!',
							message: 'Data has been saved!'
						});
					}, function(){
						$scope.processing = false;
						$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
					});
				}
			}
		};

		$scope.delete = function(username){
			if(!$scope.processing){
				$scope.processing = true;
				$rootScope.$broadcast(CONFIG.EVENTS.showLoading);
				YoutubeService.deleteRecord(username).then(function(){
					$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
					$scope.processing = false;
					CacheService.removeAll();
					$scope.getList(lastParams);
				}, function(){
					$scope.processing = false;
					$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
				});
			}
		};

		$scope.get = function(username){
			if(!$scope.processing){
				$scope.processing = true;
				$rootScope.$broadcast(CONFIG.EVENTS.showLoading);
				YoutubeService.getRecord(username).then(function(res){
					$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
					$scope.processing = false;
					$scope.youtube = {
						username: res.data.name,
						credentials: JSON.parse(atob(res.data.credentials))
					};
					$scope.isEdit = true;
				}, function(){
					$scope.processing = false;
					$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
				});
			}
		};

		var init = function() {
			$scope.getList($scope.defaultParams);
		};

		init();

	}]);