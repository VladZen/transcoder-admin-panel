'use strict';

angular.module('transcoder')
	.service('YoutubeService',['API', 'CONFIG', function(API, CONFIG){

	var YoutubeService = {};

	YoutubeService.list = function(params){
		return API.post(CONFIG.API_URLS.YOUTUBE.list, params);
	};

	YoutubeService.getRecord = function(username){
		return API.get(CONFIG.API_URLS.YOUTUBE.record + username);
	};

	YoutubeService.newRecord = function(params){
		return API.post(CONFIG.API_URLS.YOUTUBE.record + params.username, {credentials: params.credentials});
	};

	YoutubeService.updateRecord = function(params){
		return API.put(CONFIG.API_URLS.YOUTUBE.record + params.username, {credentials: params.credentials});
	};

	YoutubeService.deleteRecord = function(username){
		return API.delete(CONFIG.API_URLS.YOUTUBE.record + username);
	};

	return YoutubeService;

}]);
