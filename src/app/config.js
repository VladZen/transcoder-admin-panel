'use strict';

angular.module('transcoder').constant('CONFIG', {
	API_URLS: {
		base: 'http://192.168.2.4:3000',
		DASHBOARD: '/dashboard',
		AUTH: {
			signIn: '/auth/signin',
			logOut: '/auth/logout'
		},
		WORKERS: {
			getList: '/statistics/workers/list',
			workerData: '/statistics/worker/',
			workerListSearch: '/statistics/workers/search',
			worker: '/worker'
		},
		TASKS: {
			task: '/task/',
			getCurrent: '/statistics/tasks/',
			stop: '/task/stop',
			edit: '/task/edit',
			delete: '/task/cancel/',
			restart: '/task/restart',
			records: '/task/records',
			getAllActive: '/statistics/task/all',
			create: '/task/create',
			createBoost: '/task/create_boost',
			chainTo: '/task/chain_to/',
			createChain: '/task/create_chain',
			createMultipleChain: '/task/multiple_inputs_chain'
		},
		SETTINGS: '/settings',
		STORAGE: {
			root: '/storage',
			getRecords: '/storage/records',
			searchRecords: '/storage/records/search',
			upload: '/storage/upload/',
      		getAll: '/storage/all/'
		},
		QUEUE_MANAGER: {
			stats: '/api/stats',
			jobs: '/api/jobs/'
		},
		TEMPLATE: {
			template: '/template/template/',
			list: '/template/list',
			all: '/template/all'
		},
		SCHEMA: {
			schema: '/schema/schema/',
			list: '/schema/list',
			all: '/schema/all'
		},
		YOUTUBE: {
			list: '/youtube/list',
			record: '/youtube/record/'
		}
	},
	TASK_STATES: {
		active: 'active',
		failed: 'failed',
		chained: 'delayed',
		complete: 'complete',
		queued: 'inactive'
	},
	EVENTS: {
		showLoading: 'loadingBarShow',
		hideLoading: 'loadingBarHide',
		showContent: 'showContent'
	},
	DATEPICKER: {
		format: 'dd-MMMM-yyyy HH:mm',
		opened: false,
		closeText: 'Close',
		options: {
			formatYear: 'yy',
			startingDay: 1,
			class: 'datepicker'
		}
	},
	DASHBOARD_PLOT_RANGE: 60,
	DASHBOARD_PLOT_FILTER: 5,
	WORKER_VIEW_PLOT_FILTER: 100,
	SERVER_POLLING_PERIOD: 3000,
	PAGINATION_LIMIT: 20,
	PAGINATION_MAX_PAGE_LINKS: 20,
	INSTANCE_TYPE_SETTINGS: [ 't1.micro', 'm1.small', 'm1.medium', 'm1.large', 'm1.xlarge', 'm3.medium', 'm3.large', 'm3.xlarge', 'm3.2xlarge', 't2.micro',
	't2.small', 't2.medium', 'm2.xlarge', 'm2.2xlarge', 'm2.4xlarge', 'cr1.8xlarge', 'i2.xlarge', 'i2.2xlarge', 'i2.4xlarge', 'i2.8xlarge', 'hi1.4xlarge',
	'hs1.8xlarge', 'c1.medium', 'c1.xlarge', 'c3.large', 'c3.xlarge', 'c3.2xlarge', 'c3.4xlarge', 'c3.8xlarge', 'c4.large', 'c4.xlarge', 'c4.2xlarge',
	'c4.4xlarge', 'c4.8xlarge', 'cc1.4xlarge', 'cc2.8xlarge', 'g2.2xlarge', 'cg1.4xlarge', 'r3.large', 'r3.xlarge', 'r3.2xlarge', 'r3.4xlarge', 'r3.8xlarge',
	'd2.xlarge', 'd2.2xlarge', 'd2.4xlarge', 'd2.8xlarge' ]
});
