'use strict';

angular.module('transcoder')
	.service('SettingsService',['API', 'CONFIG', function(API, CONFIG){

	var SettingsService = {};

	SettingsService.get = function(){
		return API.get(CONFIG.API_URLS.SETTINGS);
	};

	SettingsService.change = function(params){
		return API.post(CONFIG.API_URLS.SETTINGS, params);
	};

	return SettingsService;

}]);
