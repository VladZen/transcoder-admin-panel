'use strict';

angular.module('transcoder')
	.controller('SettingsCtrl', ['$scope', '$q', 'CacheService', 'CONFIG', 'ModalAlert', 'SettingsService', '$timeout', '$rootScope', function($scope, $q, CacheService, CONFIG, ModalAlert, SettingsService, $timeout, $rootScope) {

		$scope.instance_types = CONFIG.INSTANCE_TYPE_SETTINGS;

		var getSettings = function() {
			var cacheID, data, deferred;
			cacheID = 'Settings';
			deferred = $q.defer();
			data = CacheService.get(cacheID);

			$rootScope.$broadcast(CONFIG.EVENTS.showLoading);

			if (data != null) {
				$timeout(function(){
					$scope.settings = angular.copy(data.data);
					$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
					$rootScope.$broadcast(CONFIG.EVENTS.showContent);
				}, 100);
			} else {
				SettingsService.get().then(function(res){
					CacheService.put(cacheID, res);
					$scope.settings = angular.copy(res.data);
					$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
					$rootScope.$broadcast(CONFIG.EVENTS.showContent);
				});
			}
		};

		$scope.slider_queue = {
			min: 1,
			max: 1000,
			step: 1
		};

		$scope.slider_max_workers = {
			min: 1,
			max: 100,
			step: 1
		};

		$scope.saveSettings = function() {
			$rootScope.$broadcast(CONFIG.EVENTS.showLoading);
			SettingsService.change($scope.settings).then(function(res){
				CacheService.removeAll();
				$scope.settings = angular.copy(res.data);
				$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
				ModalAlert.show({
					title: 'Success!',
					message: 'Settings has been successfully updated!'
				});
			});
		};

		var init = function() {
			getSettings();
		};

		init();

	}]);