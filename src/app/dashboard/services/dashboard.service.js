'use strict';

angular.module('transcoder')
	.service('DashboardService',['API', 'CONFIG', function(API, CONFIG){

	var DashboardService = {};

	DashboardService.statistics = function(){
		return API.get(CONFIG.API_URLS.DASHBOARD);
	};

	return DashboardService;

}]);
