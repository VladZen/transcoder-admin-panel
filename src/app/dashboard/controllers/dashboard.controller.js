'use strict';
/*globals console, _ */

angular.module('transcoder')
	.controller('DashboardCtrl', ['$scope', 'WorkersService', 'DashboardService', 'SomeUtils', '$interval', '$q', 'CacheService', 'CONFIG', '$rootScope', '$timeout', 'moment', function($scope, WorkersService, DashboardService, SomeUtils, $interval, $q, CacheService, CONFIG, $rootScope, $timeout, moment) {

		var workersRequestParams = {
			skip: 0,
			limit: CONFIG.PAGINATION_LIMIT
		};

    $scope.mongoAlive = false;
    $scope.redisAlive = false;

		var statTrigger = false,
			plotGetTrigger = false,
			plotRefreshTrigger = false,
			listWorkersTrigger = false,
			refreshPlot,refreshStatistics,refreshWorkers;

		var plotGlobalOptions =	{
		  colors: ['#23b7e5', '#7266ba', '#A8DB37'],
		  series: { shadowSize: 3 },
		  xaxis:{ mode: 'time', font: { color: '#a1a7ac' } },
		  yaxis:{ font: { color: '#a1a7ac' } },
		  grid: { hoverable: true, clickable: true, borderWidth: 0, color: '#dce5ec' },
		  tooltip: true,
		  tooltipOpts: { content: 'Visits of %x.1 is %y.4',  defaultTheme: false, shifts: { x: 10, y: -25 } }
		};

		var updatePlotView = function(cpu, hdd, ram) {
			$scope.plot = [
				[
			      { data: cpu, label:'CPU', points: { show: false, radius: 1}, splines: { show: true, tension: 0.4, lineWidth: 1, fill: 1} },
			      { data: hdd, label:'HDD', points: { show: false, radius: 1}, splines: { show: true, tension: 0.4, lineWidth: 1, fill: 0 } },
			      { data: ram, label:'RAM', points: { show: false, radius: 1}, splines: { show: true, tension: 0.4, lineWidth: 1, fill: 0.1 } },
			    ],
			    plotGlobalOptions
			];
		};

		$scope.plot = [];

		$scope.getWorkers = function(params) {
			var cacheID, data, deferred;
			deferred = $q.defer();
			data = CacheService.get(cacheID);
			cacheID = 'WorkersList' + JSON.stringify(params);


			$rootScope.$broadcast(CONFIG.EVENTS.showLoading);

			if (data) {
				$timeout(function(){
					$scope.workers = data.data.workers;
					$scope.seeWorkerStat(data.data.workers[0].worker_id);
					$scope.pagination = {
						skip: params.skip,
						limit: params.limit,
						total: data.data.total
					};
				}, 100);
			} else {
				$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
				refreshWorkers = $interval(function(){
					if(!listWorkersTrigger){
						listWorkersTrigger = true;
						WorkersService.list(params).then(function(res){
							$scope.workers = res.data.workers;

							if($scope.workers.length > 0){
								CacheService.put(cacheID, res);
								$interval.cancel(refreshWorkers);
								$scope.seeWorkerStat(res.data.workers[0].worker_id);
								$rootScope.$broadcast(CONFIG.EVENTS.showLoading);
							}

							$scope.pagination = {
								skip: params.skip,
								limit: params.limit,
								total: res.data.total
							};

							listWorkersTrigger = false;
						});
					}
				}, CONFIG.SERVER_POLLING_PERIOD);
			}
		};

		$scope.seeWorkerStat = function(id) {

			if($scope.activeWorker !== id && !plotGetTrigger){

				$rootScope.$broadcast(CONFIG.EVENTS.showLoading);

				if (refreshPlot) {
					$interval.cancel(refreshPlot);
				}

				$scope.activeWorker = id;

				var timeRange = {
					begin: moment().subtract(CONFIG.DASHBOARD_PLOT_RANGE, 'minute').utc().format(),
					end: moment().utc().format(),
					filter: CONFIG.DASHBOARD_PLOT_FILTER
				};

				plotGetTrigger = true;

				WorkersService.getWorkerStat(timeRange, id).then(function(res){
					console.log(res);
					var sorted = _.sortBy(res.data, 'datetime'),
						cpu = [],
						hdd = [],
						ram = [];

					_.forEach(sorted, function(key) {
						var time = new Date(SomeUtils.trueUTC(key.datetime));
						cpu.push([time,key.cpu_usage]);
						hdd.push([time,key.disk_usage_percent]);
						ram.push([time,key.ram_usage_percent]);
					});

					updatePlotView(cpu, hdd, ram);

					refreshPlot = $interval(function(){

						if(!plotRefreshTrigger){
							plotRefreshTrigger = true;
							WorkersService.updateWorkerStat(id).then(function(res){

								_.forEach(res.data, function(key) {
									var time = new Date(SomeUtils.trueUTC(key.datetime));
									cpu.push([time,key.cpu_usage]);
									cpu.splice(0,1);
									hdd.push([time,key.disk_usage_percent]);
									hdd.splice(0,1);
									ram.push([time,key.ram_usage_percent]);
									ram.splice(0,1);
								});

								updatePlotView(cpu, hdd, ram);

								plotRefreshTrigger = false;
								return plotRefreshTrigger;
							});
						}
					}, CONFIG.SERVER_POLLING_PERIOD);

					$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);

					plotGetTrigger = false;
					return plotGetTrigger;

				});
			} else {
				$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
			}

		};

		var init = function() {
			$scope.getWorkers(workersRequestParams);

			refreshStatistics = $interval(function(){
				if(!statTrigger){
					statTrigger = true;
					DashboardService.statistics().then(function(res){
            console.log(res);
            $scope.mongoAlive = true;

            // does not really works - UI redirects to login page "not authenticated" because no access to redis
            $scope.redisAlive = true;  

						$scope.statistics = res.data;

						$rootScope.$broadcast(CONFIG.EVENTS.showContent);

						var white = $scope.statistics.proportion.white,
							gold = $scope.statistics.proportion.gold;

						$scope.sparkline = [
							[white/gold, (gold - (white / gold) * gold ) > 0 ? gold - (white / gold) * gold : 0],
							{
								type:'pie',
								height:40,
								sliceColors:['#fad733','#fff']
							}
						];
						statTrigger = false;
						return statTrigger;
					})
          .catch(function(error){
            $scope.mongoAlive = (error.data.error.name !== 'MongoError');
          });
				}

			}, CONFIG.SERVER_POLLING_PERIOD);
		};

		init();

		$scope.$on('$destroy', function() {
          $interval.cancel(refreshStatistics);
          $interval.cancel(refreshPlot);
          $interval.cancel(refreshWorkers);
        });


	}]);
