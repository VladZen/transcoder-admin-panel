'use strict';

angular.module('transcoder')
	.controller('LoginCtrl', ['$scope', '$state', '$cookies', 'AuthService', function ($scope, $state, $cookies, AuthService) {
		$scope.logIn = function () {
			AuthService.login($scope.user).then(function(res){
				$cookies['connect.sid'] = res.data.cookies;
				$state.go('inside.dashboard');
			});
		};
	}]);
