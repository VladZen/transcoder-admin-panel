'use strict';

angular.module('transcoder', [
	'ngAnimate',
	'ngCookies',
  'ngLodash',
	'ngTouch',
	'ngSanitize',
	'ui.router',
	'ui.bootstrap',
    'ui.utils',
    'js-data',
    'angularMoment',
    'ui.bootstrap-slider',
    'angularFileUpload',
    'angular-json-editor'
	])
	.config(['$httpProvider', 'CONFIG', 'DSProvider', function ($httpProvider, CONFIG, DSProvider) {

    	//CORS configuration
    	$httpProvider.defaults.useXDomain = true;
    	$httpProvider.defaults.withCredentials = true;
    	delete $httpProvider.defaults.headers.common['X-Requested-With'];

    	//fore resurses
    	DSProvider.defaults.basePath = CONFIG.API_URLS.base;

    	$httpProvider.interceptors.push(['$q', '$cookies', '$injector',function($q, $cookies, $injector) {
			return {
				'response': function(response) {
					return response;
				},
				'responseError': function(rejection) {
					$injector.invoke(['$state', 'ModalAlert', '$rootScope', function($state, ModalAlert, $rootScope) {
						$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);

						var alertMessage = function(text, func){
							ModalAlert.show({
								title: 'Error!',
								message: text,
								ok: function() {
									return func !== undefined ? func : 'ok!';
								}
							});
						};

						if(rejection.data != null && rejection.data.code) {
							if(rejection.data.code === 218003){
								delete $cookies['connect.sid'];
								alertMessage('You are not authenticated!', $state.go('login'));
							} else {
								alertMessage(rejection.data.message);
							}
						} else {
							delete $cookies['connect.sid'];
							alertMessage('Sorry, there are problems on server.', $state.go('login'));
						}
					}]);
					return $q.reject(rejection);
				}
			};
		}]);

	}])
	.run(['$rootScope', 'CONFIG', '$q', '$state', '$cookies', function($rootScope, CONFIG, $q, $state, $cookies) {

		if($state.current.name !== 'login' && $cookies['connect.sid'] === null){
			$state.go('login');
		}

		$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState){
			if($cookies['connect.sid'] === null && toState.name !== 'login'){
				$state.go('login');
			}
			if($cookies['connect.sid'] === null && fromState.name === 'login'){
				event.preventDefault();
			}
		});
	}])

;
