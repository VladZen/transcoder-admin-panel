'use strict';

angular.module('transcoder').config(['$stateProvider', '$urlRouterProvider', 'CONFIG', function ($stateProvider, $urlRouterProvider, CONFIG) {

    $stateProvider

      .state('login', {
        url: '/login',
        templateUrl: '/app/login/views/login.html',
        controller: 'LoginCtrl'
      })

      .state('inside', {
        url: '/',
        templateUrl: '/app/common/views/inside.skelet.view.html',
        abstract: true
      })

      .state('inside.dashboard', {
        url: 'dashboard',
        templateUrl: '/app/dashboard/views/dashboard.html',
        controller: 'DashboardCtrl'
      })

      .state('inside.workers', {
        url: 'workers',
        abstract: true,
        templateUrl: '/app/workers/views/workers.html'
      })

      .state('inside.workers.list', {
        url: '/list',
        controller: 'WorkerslistCtrl',
        templateUrl: '/app/workers/views/workers.all.html'
      })

      .state('inside.workers.worker_view', {
        url: '/:worker_id',
        controller: 'WorkerInsideCtrl',
        templateUrl: '/app/workers/views/workers.one.html'
      })

      .state('inside.storage', {
        url: 'storage',
        controller: 'StorageCtrl',
        templateUrl: '/app/storage/views/storage.html'
      })

      .state('inside.tasks', {
        url: 'tasks',
        abstract: true,
        templateUrl: '/app/tasks/views/tasks.html'
      })

      .state('inside.tasks.new', {
        url: '/create',
        controller: 'TaskNewCtrl',
        templateUrl: '/app/tasks/views/task.create_edit.html'
      })

      .state('inside.tasks.edit', {
        url: '/edit/:task_id',
        controller: 'TaskEditCtrl',
        templateUrl: '/app/tasks/views/task.create_edit.html'
      })

      .state('inside.tasks.list', {
        url: '',
        abstract: true,
        controller: 'TasksListCtrl',
        templateUrl: '/app/tasks/views/task.list.html'
      })

      .state('inside.tasks.list.active', {
        url: '/active',
        templateUrl: '/app/tasks/views/partials/task_table.html',
        data:{
          activeStatus: CONFIG.TASK_STATES.active
        }
      })

      .state('inside.tasks.list.completed', {
        url: '/completed',
        templateUrl: '/app/tasks/views/partials/task_table.html',
        data:{
          activeStatus: CONFIG.TASK_STATES.complete
        }
      })

      .state('inside.tasks.list.failed', {
        url: '/failed',
        templateUrl: '/app/tasks/views/partials/task_table.html',
        data:{
          activeStatus: CONFIG.TASK_STATES.failed
        }
      })

      .state('inside.tasks.list.chained', {
        url: '/chained',
        templateUrl: '/app/tasks/views/partials/task_table.html',
        data:{
          activeStatus: CONFIG.TASK_STATES.chained
        }
      })

      .state('inside.tasks.list.queued', {
        url: '/queued',
        templateUrl: '/app/tasks/views/partials/task_table.html',
        data:{
          activeStatus: CONFIG.TASK_STATES.queued
        }
      })

      .state('inside.settings', {
        url: 'settings',
        controller: 'SettingsCtrl',
        templateUrl: '/app/settings/views/settings.html'
      })

      .state('inside.youtube', {
        url: 'youtube',
        controller: 'YoutubeCtrl',
        templateUrl: '/app/youtube/views/youtube.html'
      })

      ;
    $urlRouterProvider
    .when('/', '/dashboard')
    .when('/tasks', '/tasks/active')
    .when('/workers', '/workers/list')
    .otherwise('/dashboard');
  }])
;