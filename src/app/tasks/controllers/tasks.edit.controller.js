'use strict';

angular.module('transcoder')
	.controller('TaskEditCtrl',['$window', '$scope', 'CONFIG', '$rootScope', 'CacheService', '$stateParams', '$q', 'TasksService', 'ModalAlert', '$state', function ($window, $scope, CONFIG, $rootScope, CacheService, $stateParams, $q, TasksService, ModalAlert, $state) {

		$scope.isCreate = false;

		var task_id = $stateParams.task_id;

		var sendPromise = function(body){
			return TasksService.editTask(body);
		};

		$scope.cancel = function(){
			$window.history.back();
		};

		$scope.send = function(params){
			$rootScope.$broadcast(CONFIG.EVENTS.showLoading);
			var body = JSON.parse(params);
			angular.extend($scope.task_settings.data, body);
			sendPromise($scope.task_settings.data).then(function(){
				$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
				ModalAlert.show({
					title: 'Success!',
					message: 'Task info has been successfully changed!'
				});
			});
		};

		$scope.send_and_run = function(params){
			$rootScope.$broadcast(CONFIG.EVENTS.showLoading);
			var body = JSON.parse(params);

			angular.extend($scope.task_settings.data, body);
			sendPromise($scope.task_settings.data).then(function(){
				$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
				$scope.sending = true;
				$rootScope.$broadcast(CONFIG.EVENTS.showLoading);
				TasksService.restartTask({task_id: task_id}).then(function () {
					$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
					$state.go('inside.tasks.list.active');
				}, function(){
					$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
					$scope.sending = false;
				});

			});
		};

    // INIT
    $rootScope.$broadcast(CONFIG.EVENTS.showLoading);
    TasksService.getById(task_id).then(function(res){
      $scope.task_settings = angular.copy(res.data);
      $scope.task_settings.data.inputs = $scope.task_settings.data.input;
      $scope.task_settings.data.outputs = $scope.task_settings.data.output;
      delete $scope.task_settings.data.input;
      delete $scope.task_settings.data.output;

      var out;
      out = angular.copy($scope.task_settings.data);
      delete out.task_id;
      $scope.taskString = JSON.stringify(out, null, 4);
      $rootScope.$broadcast(CONFIG.EVENTS.showContent);
      $rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
    });

}]);
