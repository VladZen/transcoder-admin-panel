'use strict';
/*globals btoa */
/*jshint unused:false*/

angular.module('transcoder')
  .controller('TaskNewCtrl',['$scope', '$rootScope', 'CONFIG', 'QueueManagerService', '$interval', 'CacheService', '$timeout', '$state', '$q', 'TasksService', 'TemplateService', 'ModalAlert', 'SchemaService', function (
        $scope, $rootScope, CONFIG, QueueManagerService, $interval, CacheService, $timeout, $state, $q, TasksService, TemplateService, ModalAlert, SchemaService) {


    var processedJSON = function(body){
      try {
        return JSON.parse(body);
      } catch(e) {
        ModalAlert.show({
          title: 'Parse error!',
          message: 'Check your settings body please and try again'
        });
        triggersOff();
      }
    };

    var triggersOff = function(){
      $rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
      $scope.sending = false;
      return $scope.sending;
    };

    $scope.isCreate = true;

    $scope.saveTplForm = {
      show: false
    };

    $scope.tpl = {};


    //CONTROL BUTTONS

    var taskProccesser = function (body, task) {
      if(!$scope.sending){
        $scope.sending = true;
        $rootScope.$broadcast(CONFIG.EVENTS.showLoading);
        var params = processedJSON(body);
        if(params){
          for (var i = 0; i < params['outputs'].length; i++) {
            if(params['outputs'][i]['youtube'] && typeof params['outputs'][i]['youtube']['credentials'] === 'object'){
              params['outputs'][i]['youtube']['credentials'] = btoa(JSON.stringify(params['outputs'][i]['youtube']['credentials']));
            }
          }
          TasksService[task](params).then(function(){
            ModalAlert.show({
              title: 'Success!',
              message: 'Task has been successfully created!'
            });
            triggersOff();
          }, function(){
            triggersOff();
          });
        }
      }
    };

    $scope.commonTask = function(body){
      taskProccesser(body, 'createCommonTask');
    };

    $scope.boostTask = function(body){
      taskProccesser(body, 'createBoostTask');
    };

    $scope.createChain = function(body){
      taskProccesser(body, 'createChain');
    };

    $scope.createMultipleChain = function(body){
      taskProccesser(body, 'createMultipleChain');
    };

    $scope.addToChain = function (taskTo, body) {
      if(!$scope.sending){
        $scope.sending = true;
        $rootScope.$broadcast(CONFIG.EVENTS.showLoading);
        var params = processedJSON(body);
        if(params){
          TasksService.addToChain(taskTo, params).then(function(){
            ModalAlert.show({
              title: 'Success!',
              message: 'Task has been successfully added to chain with'+ taskTo +'!'
            });
            triggersOff();
          }, function(){
            triggersOff();
          });
        }
      }
    };

    // SCHEMA
    $scope.schemaShow = true;
    $scope.forms = {};
    $scope.forms.schema = {
      show: false
    };
    $scope.settings = {};

    function schemaFormSucceded(){
      triggersOff();
      $scope.schemaName = '';
      $scope.forms.schema.show = false;
    }

    $scope.reinitJsonEditor = function(cb){
      if (!$scope.schemaShow) {return;}
      $scope.schemaShow = false;
      if (angular.isFunction(cb)){
        cb();
      }
      $timeout(function(){
        $scope.schemaShow = true;
      }, 0);
    };

    $scope.chooseSchema = function(){
      $scope.reinitJsonEditor(function(){
        $scope.editorSchema = $scope.chosenSchema.body;
      });
    };

    $scope.onCreateSchema = function(){
      $scope.schemaPrototype=null; 
      $scope.settings = '{}';
      $scope.forms.schema.show = true;
    };

    $scope.createSchema = function(){
      $rootScope.$broadcast(CONFIG.EVENTS.showLoading);

      SchemaService.create({
        name: $scope.schemaName,
        body: JSON.parse($scope.settings)
      }).then(function(res){
        return $scope.loadSchemas();
      }).then(schemaFormSucceded) ;
    };


    $scope.updateSchema = function(){
      $rootScope.$broadcast(CONFIG.EVENTS.showLoading);

      SchemaService.edit({
        name: $scope.schemaName,
        body: JSON.parse($scope.settings)
      }).then(function(res){
        return $scope.loadSchemas();
      }).then(schemaFormSucceded);
    };


    $scope.deleteSchema = function(name){
      $rootScope.$broadcast(CONFIG.EVENTS.showLoading);
      SchemaService.delete(name).then(function(){
        return $scope.loadSchemas();
      }).then(triggersOff);
    };

    $scope.onEditSchema = function(schema){
      $scope.schemaPrototype = schema;
      $scope.settings = JSON.stringify(schema.body, '', 4);
      $scope.schemaName = schema.name;
      $scope.forms.schema.show = true;
    };

    $scope.task = {};
    $scope.taskString = '{}';

    $scope.builderChange = function(data){
      $scope.task = data;
      $scope.taskString = JSON.stringify($scope.task, '', 4);
    };
    
    $scope.loadSchemas = function(){
      return SchemaService.all().then(function(res){
        $scope.schemas = res.data;
        angular.forEach($scope.schemas, function(schema){
          schema.body = JSON.parse(schema.body);
        });
        $scope.reinitJsonEditor(function(){
          var i = 0,
              len = $scope.schemas.length;
          while (i < len && $scope.schemas[i].name !== 'create'){i++;}
          $scope.chosenSchema = $scope.schemas[i];
          $scope.editorSchema = $scope.chosenSchema.body;
          triggersOff();
        });
      });
    };

    //TEMPLATE
    $scope.loadTemplates = function(){
      return TemplateService.all().then(function(res){
        $scope.templates = res.data;
        triggersOff();
      });
    };

    var typeChoose = false;
    $scope.chooseTemplate = function(name){
      if(typeChoose || !$scope.schemaShow) {return;}
      $scope.schemaShow = false;

      $rootScope.$broadcast(CONFIG.EVENTS.showLoading);
      typeChoose = true;
      $scope.gotTemplate = false;
      TemplateService.get(name).then(function(res){
        $rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
        $scope.gotTemplate = true;
        typeChoose = false;
        $scope.task = processedJSON(res.data.body);
        $scope.taskString = JSON.stringify($scope.task, '', 4);
        $timeout(function(){
          $scope.schemaShow = true;
        }, 0);
        $scope.saveTplForm = {
          show: false,
          status: ''
        };
        $scope.chainTo = false;
        $scope.taskTo = '';
      });
    };

    $scope.editTemplate = function(params) {
      $scope.saveTplForm = {
        show: true,
        status: 'edit'
      };
      $scope.tpl = {
        body: JSON.stringify(processedJSON(params.body), '', 4),
        name: params.name
      };
    };

    $scope.deleteTemplate = function(name) {
      if(!$scope.sending){
        $scope.sending = true;
        $rootScope.$broadcast(CONFIG.EVENTS.showLoading);
        $scope.tpl = {};
        $scope.saveTplForm = {
          show: false,
          status: 'edit'
        };
        TemplateService.delete(name).then(function(){
          TemplateService.all().then(function(res){
            $scope.templates = res.data;
            triggersOff();
          });
        }, function(){
          triggersOff();
        });
      }
    };

    $scope.tplProcess = function(tpl) {
      var params;
      if($scope.sending) {return;}
      $scope.sending = true;
      $rootScope.$broadcast(CONFIG.EVENTS.showLoading);
      if($scope.saveTplForm.status === 'create'){
        params = processedJSON($scope.taskString);
        if(params){
          TemplateService.create({
            name: tpl.name,
            body: params
          })
          .then(function(res){
            $scope.saveTplForm = false;
            TemplateService.all().then(function(res){
              $scope.templates = res.data;
              triggersOff();
            });
          }, function(){
            triggersOff();
          });
        }
      } else {
        params = processedJSON(tpl.body);
        if(params){
          TemplateService.edit({
            name: tpl.name,
            body: params
          })
          .then(function(res){
            $scope.saveTplForm = false;
            TemplateService.all().then(function(res){
              $scope.templates = res.data;
              triggersOff();
            }, function(){
              triggersOff();
            });
          });
        }
      }
    };

    // INIT
    $scope.editorSchema = {};

    $rootScope.$broadcast(CONFIG.EVENTS.showLoading);
    $q.all([$scope.loadSchemas(), $scope.loadTemplates()]).then(function(){
      $rootScope.$broadcast(CONFIG.EVENTS.showContent);
    });
}]);
