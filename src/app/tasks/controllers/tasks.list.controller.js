'use strict';
/*globals _ */

angular.module('transcoder')
	.controller('TasksListCtrl',['$filter', '$scope', '$rootScope', 'CONFIG', 'QueueManagerService', '$interval', '$state', '$q', 'TasksService', 'ModalAlert', function ($filter, $scope, $rootScope, CONFIG, QueueManagerService, $interval, $state, $q, TasksService, ModalAlert) {

		//defaults
		$scope.defaults = {
      skip: 0,
      limit: CONFIG.PAGINATION_LIMIT,
      taskDetailsUrl: '/app/tasks/views/partials/info.popup.html'
		};
		var lastParams;
		$scope.activeStatus = $state.current.data.activeStatus;

		//triggers
		var getStatsTrigger = false,
			getTasksTrigger = false;

		$scope.copyTask = function(task) {
			var html = '<pre class="task-details">'+ $filter('json')(angular.copy(task)) +'</pre>';

			ModalAlert.show({
				title: 'Details',
				message: html,
				size: 'md'
			});
		};

		//polling intervals
		var getStatsInterval, getTasksInterval;

		//active status

		$scope.loadingState = true;
		$rootScope.$on('$stateChangeStart', function(event, toState){
			if(toState.data && toState.data.activeStatus){
				$scope.tasks = [];
				$scope.activeStatus = toState.data.activeStatus;
				init();
			} else {
				$interval.cancel(getStatsInterval);
				$interval.cancel(getTasksInterval);
			}
		});

		//get current status tasks
		$scope.getTasks = function(params){

			$interval.cancel(getTasksInterval);

			if(!_.isEqual(lastParams, params)){
				lastParams = params;
				$scope.loadingState = true;
			}

			var func = function(){
				params.state = $scope.activeStatus;
				if(!getTasksTrigger){
					getTasksTrigger = true;
					TasksService.getTasksList(params).then(function(res){
            if (!angular.equals($scope.tasks, res.data)) {
              $scope.tasks = res.data;
            }
						$scope.loadingState = false;
						getTasksTrigger = false;
						$scope.activeAction = false;
						$scope.pagination = {
							skip: params.skip,
							limit: CONFIG.PAGINATION_LIMIT,
							total: $scope.stats[params.state + 'Count']
						};
					});
				}
			};

			getTasksInterval = $interval(func, CONFIG.SERVER_POLLING_PERIOD);

			return lastParams;
		};

		//get totals of all types of tasks
		$scope.getStats = function(){

			$interval.cancel(getStatsInterval);

			var func1 = function(){
				if(!getStatsTrigger){
					getStatsTrigger = true;
					QueueManagerService.stats().then(function(res){

						$scope.stats = res.data;


						$rootScope.$broadcast(CONFIG.EVENTS.showContent);
						$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
						getStatsTrigger = false;
					});
				}
			};

			getStatsInterval = $interval(function(){func1();}, CONFIG.SERVER_POLLING_PERIOD);

		};

		//ACTION TASKS

		$scope.activeAction = false;

		var processAction = function(action, task_id) {
			if(!$scope.activeAction){
				$scope.activeAction = true;
				$rootScope.$broadcast(CONFIG.EVENTS.showLoading);
				TasksService[action]({task_id: task_id}).then(function () {
					$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
					$scope.getTasks(lastParams);
				}, function(){
					$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
					$scope.activeAction = false;
				});
			}
		};

		//stop
		$scope.stop = function(task_id){
			processAction('stopTask', task_id);
		};

		//delete
		$scope.delete = function(task_id){
			processAction('deleteTask', task_id);
		};

		//restart
		$scope.restart = function(task_id){
			processAction('restartTask', task_id);
		};

		var init = function() {
			$rootScope.$broadcast(CONFIG.EVENTS.showLoading);
			$scope.getStats();
			$scope.getTasks($scope.defaults);
			$scope.loadingState = true;
			$scope.pagination = {
				skip: 0,
				limit: CONFIG.PAGINATION_LIMIT,
				total: 0
			};
		};

		init();

		$scope.$on('$destroy', function() {
        	$interval.cancel(getStatsInterval);
        	$interval.cancel(getTasksInterval);
        });

	}]);
