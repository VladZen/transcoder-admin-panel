'use strict';

angular.module('transcoder')
	.service('QueueManagerService',['CONFIG', 'API', function(CONFIG, API){

	var QueueManagerService = {};

	QueueManagerService.stats = function(){
		return API.get(CONFIG.API_URLS.QUEUE_MANAGER.stats);
	};

	return QueueManagerService;

}]);
