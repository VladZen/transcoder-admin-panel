'use strict';

angular.module('transcoder')
	.service('SchemaService', ['CONFIG', 'API', function(CONFIG, API){

  return {
    get: function(name){
  		return API.get(CONFIG.API_URLS.SCHEMA.schema + name);
  	},
  
    all: function(){
  		return API.get(CONFIG.API_URLS.SCHEMA.all);
  	},
  
    create: function(params){
  		return API.post(CONFIG.API_URLS.SCHEMA.schema, params);
  	},
  
    edit: function(params){
  		return API.put(CONFIG.API_URLS.SCHEMA.schema, params);
  	},
  
    delete: function(name){
  		return API.delete(CONFIG.API_URLS.SCHEMA.schema + name);
  	}
  };

}]);
