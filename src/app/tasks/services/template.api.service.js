'use strict';

angular.module('transcoder')
	.service('TemplateService',['CONFIG', 'API', function(CONFIG, API){

	var TemplateService = {};

	TemplateService.get = function(name){
		return API.get(CONFIG.API_URLS.TEMPLATE.template + name);
	};

	TemplateService.all = function(){
		return API.get(CONFIG.API_URLS.TEMPLATE.all);
	};

	TemplateService.create = function(params){
		return API.post(CONFIG.API_URLS.TEMPLATE.template, params);
	};

	TemplateService.edit = function(params){
		return API.put(CONFIG.API_URLS.TEMPLATE.template, params);
	};

	TemplateService.delete = function(name){
		return API.delete(CONFIG.API_URLS.TEMPLATE.template + name);
	};

	return TemplateService;

}]);
