'use strict';
/*globals console, data*/
/*jshint unused:false*/

angular.module('transcoder')
	.controller('StorageCtrl', ['$scope', '$q', 'CONFIG', 'ModalAlert', '$timeout', '$rootScope', 'StorageService', '$window', 'FileUploader', '$interval', function(
                                $scope, $q, CONFIG, ModalAlert, $timeout, $rootScope, StorageService, $window, FileUploader, $interval) {

		var workersRequestParams = {
			skip: 0,
			limit: CONFIG.PAGINATION_LIMIT
		};

		var uploader = $scope.uploader = new FileUploader({url: '/'});

    	uploader.onSuccessItem = function() {
    	    $scope.success = true;
    	    $scope.fail = false;
    	    if(lastParams.query) {
				$scope.searchFiles(lastParams);
			} else {
				$scope.getFiles(lastParams);
			}
    	};

    	uploader.onErrorItem = function() {
    		$scope.fail = true;
    		$scope.success = false;
        	ModalAlert.show({
        		title: 'Error',
        		message: 'An error occured when file downloading file. To avoid this check your filename extension and try again later.'
        	});
    	};

		var lastParams, oldQuery = '', deleteTrigger;

		var requestProcesser = function(res, params, controller){
			$scope.storageFiles = res.data.records;
			$scope.pagination = {
				skip: params.skip,
				limit: params.limit,
				total: res.data.total,
				controller: controller
			};

			lastParams = params;
			$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
		};

		$scope.getFiles = function(params) {

			$scope.resultSearch = false;
			deleteTrigger = false;
			if(!params.limit) {
				params.limit = CONFIG.PAGINATION_LIMIT;
			}
			$rootScope.$broadcast(CONFIG.EVENTS.showLoading);

			StorageService.getRecords(params).then(function(res){
				requestProcesser(res, params, $scope.getFiles);
				$rootScope.$broadcast(CONFIG.EVENTS.showContent);
			});
		};

		$scope.searchFiles = function(params){
			deleteTrigger = false;
			console.log(params);
			if(!params.limit) {
				params.limit = CONFIG.PAGINATION_LIMIT;
			}

			if(oldQuery === '' && params.query) {
				oldQuery = params.query;
			}

			if(params.query && oldQuery !== params.query){
				oldQuery = params.query;
				params = {
					skip: 0,
					limit: CONFIG.PAGINATION_LIMIT,
					query: params.query
				};
			}

			console.log(params.query, oldQuery);

			if(typeof params.query === 'undefined'){
				params.query = oldQuery;
			}

			$rootScope.$broadcast(CONFIG.EVENTS.showLoading);

			if (data != null) {
				$timeout(function(){
					requestProcesser(data, params, $scope.searchFiles);
					$scope.resultSearch = true;
				}, 100);
			} else {
				StorageService.searchRecords(params).then(function(res){
					requestProcesser(res, params, $scope.searchFiles);
					$scope.resultSearch = true;
				});
			}

		};

    $scope.deleteFile = function (task_id) {
      if(!deleteTrigger){
        $rootScope.$broadcast(CONFIG.EVENTS.showLoading);
        StorageService.deleteOneRecord(task_id).then(function(){
          deleteTrigger = true;
          CacheService.removeAll();
          if(lastParams.query) {
            $scope.searchFiles(lastParams);
          } else {
            $scope.getFiles(lastParams);
          }
          $rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
        });
      }
    };

		$scope.downloadFile = function (task_id) {
			$rootScope.$broadcast(CONFIG.EVENTS.showLoading);
			StorageService.getOneRecord(task_id).then(function(res){
				$window.open(res.data.access_url);
				$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
			});
		};

    $scope.downloadAllFiles = function (task_id) {
      $rootScope.$broadcast(CONFIG.EVENTS.showLoading);
      StorageService.getAllTaskRecords(task_id).then(function(res){
        _.forEach(res.data, function(item){
          $window.open(item.access_url);
        });
        $rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
      });
    };

		$scope.uploadFile = function(item) {
			$rootScope.$broadcast(CONFIG.EVENTS.showLoading);
			StorageService.upload(item.file.name, item.file.type).then(function(res){
				$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
				var headers = {
					'Content-Type': item.file.type
				};
				item.method = 'PUT';
				item.url = res.data.url;
				item.headers = headers;
				item.upload();
			});
		};

    $scope.pollGetFilesPromise = $interval(function(){
      $scope.getFiles(workersRequestParams);
    }, 5000);
    $scope.$on('$destroy', function(){
      if (angular.isDefined($scope.pollGetFilesPromise)){
        $interval.cancel($scope.pollGetFilesPromise);
        $scope.pollGetFilesPromise = undefined;
      }
    });

	}]);
