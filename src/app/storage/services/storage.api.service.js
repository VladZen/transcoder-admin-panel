'use strict';

angular.module('transcoder')
	.service('StorageService',['API', 'CONFIG', function(API, CONFIG){

	var StorageService = {};

	StorageService.getRecords = function(params){
		return API.post(CONFIG.API_URLS.STORAGE.getRecords, params);
	};

	StorageService.searchRecords = function(params){
		return API.post(CONFIG.API_URLS.STORAGE.searchRecords, params);
	};

	StorageService.getOneRecord = function(task_id){
		return API.get(CONFIG.API_URLS.STORAGE.root + '/' + task_id);
	};

  StorageService.getAllTaskRecords = function(task_id){
    return API.get(CONFIG.API_URLS.STORAGE.getAll + task_id);
  };

	StorageService.deleteOneRecord = function(task_id){
		return API.delete(CONFIG.API_URLS.STORAGE.root + '/' + task_id);
	};

	StorageService.upload = function(name, query){
		return API.get(CONFIG.API_URLS.STORAGE.upload + name + '?content_type=' + query);
	};

	return StorageService;

}]);
