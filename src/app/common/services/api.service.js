'use strict';


angular.module('transcoder').factory('API',['CONFIG', 'DSHttpAdapter', function(CONFIG, DSHttpAdapter) {
  var Api;
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };
  Api = (function() {
    function Api() {
      this.post = bind(this.post, this);
      this.put = bind(this.put, this);
      this['delete'] = bind(this['delete'], this);
      this.get = bind(this.get, this);
    }

    Api.prototype.url = function(slug) {
      return [CONFIG.API_URLS.base, slug].join('');
    };

    Api.prototype.get = function(slug, object) {
      var options;
      if (object === null) {
        object = {};
      }
      options = {
        params: object
      };
      return DSHttpAdapter.GET(this.url(slug), options);
    };

    Api.prototype['delete'] = function(slug, object) {
      var options;
      options = {
        params: object
      };
      return DSHttpAdapter.DEL(this.url(slug), options);
    };

    Api.prototype.put = function(slug, object, JSON) {
      return DSHttpAdapter.PUT(this.url(slug), object, {
        params: JSON
      });
    };

    Api.prototype.post = function(slug, object, JSON) {
      return DSHttpAdapter.POST(this.url(slug), object, {
        params: JSON
      });
    };

    return Api;

  })();
  return new Api();
}]);