'use strict';
angular.module('transcoder').service('ModalAlert', ['$modal', function($modal) {
	this.show = function(options) {
		var ModalInstanceCtrl, defaultOptions;
		defaultOptions = {
			title: '',
			message: '',
			ok: function() {
				return 'OK';
			},
			cancel: function() {
				return 'Cancel';
			},
			size: 'sm',
			templateUrl: '/app/common/views/partials/modal.html'
		};
		angular.extend(defaultOptions, options);
		ModalInstanceCtrl = function($scope, $modalInstance) {
			$scope.data = defaultOptions;
			$scope.ok = function() {
				$modalInstance.close();
				return defaultOptions.ok();
			};
			$scope.cancel = function() {
				$modalInstance.close();
				return defaultOptions.cancel();
			};
		};
		$modal.open({
			templateUrl: defaultOptions.templateUrl,
			controller: ['$scope', '$modalInstance', ModalInstanceCtrl],
			size: defaultOptions.size,
			windowClass: 'modal-settings'
		});
	};
}]);