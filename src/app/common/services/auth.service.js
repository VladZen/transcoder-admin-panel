'use strict';

angular.module('transcoder')
	.service('AuthService',['API', 'CONFIG', 'CacheService', function(API, CONFIG, CacheService){

	var AuthService = {};

	AuthService.login = function(params){
		return API.post(CONFIG.API_URLS.AUTH.signIn, params);
	};

	AuthService.logout = function(){
		CacheService.removeAll();
		return API.get(CONFIG.API_URLS.AUTH.logOut);
	};

	return AuthService;


}]);
