'use strict';
/*globals console */
angular.module('transcoder').factory('CacheService',['$cacheFactory', function($cacheFactory) {
  var MyCache;
  MyCache = (function() {
    function MyCache() {
      this.cacheObject = $cacheFactory('transcoder-cache');
      this.allKeys = [];
    }

    MyCache.prototype.get = function(key) {
      return this.cacheObject.get(key);
    };

    MyCache.prototype.removeAll = function() {
      this.cacheObject.removeAll();
      this.allKeys = [];
      return this.allKeys;
    };

    MyCache.prototype.put = function(key, value) {
      this.cacheObject.put(key, value);
      return this.allKeys.push(key);
    };

    MyCache.prototype.destroy = function() {
      this.cacheObject.destroy();
      this.allKeys = [];
      return this.allKeys;
    };

    MyCache.prototype.info = function() {
      return this.cacheObject.info();
    };

    MyCache.prototype.getKeys = function() {
      return this.allKeys;
    };

    MyCache.prototype.removeByMatches = function(str) {
      var allKeys, foundedKeys;
      allKeys = this.getKeys();
      foundedKeys = [];
      if (allKeys.length > 0) {
        allKeys.map(function(element) {
          if (element.indexOf(str) > -1) {
            return foundedKeys.push(element);
          }
        });
        if (foundedKeys.length > 0) {
          return foundedKeys.map((function(_this) {
            return function(el) {
              console.log('Going to remove following keys from Cache', el);
              return _this.remove(el);
            };
          })(this));
        }
      }
    };

    MyCache.prototype.remove = function(key) {
      var foundIndex;
      this.cacheObject.remove(key);
      foundIndex = this.allKeys.indexOf(key);
      if (foundIndex > -1) {
        return this.allKeys.splice(foundIndex, 1);
      }
    };

    return MyCache;

  })();
  return new MyCache();
}]);