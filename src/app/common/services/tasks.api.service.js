'use strict';

angular.module('transcoder')
	.service('TasksService',['API', 'CONFIG', function(API, CONFIG){

	var TasksService = {};

	TasksService.getCurrent = function(workerId){
		return API.get(CONFIG.API_URLS.TASKS.getCurrent + workerId);
	};

	TasksService.getById = function(task_id){
		return API.get(CONFIG.API_URLS.TASKS.task + task_id);
	};

	TasksService.stopTask = function(params){
		return API.post(CONFIG.API_URLS.TASKS.stop, params);
	};

	TasksService.deleteTask = function(params){
		return API.post(CONFIG.API_URLS.TASKS.delete, params);
	};

	TasksService.createCommonTask = function(params){
		return API.post(CONFIG.API_URLS.TASKS.create, params);
	};

	TasksService.createBoostTask = function(params){
		return API.post(CONFIG.API_URLS.TASKS.createBoost, params);
	};

	TasksService.addToChain = function(taskTo, params){
		return API.post(CONFIG.API_URLS.TASKS.chainTo + taskTo, params);
	};

	TasksService.createChain = function(params){
		return API.post(CONFIG.API_URLS.TASKS.createChain, params);
	};

	TasksService.createMultipleChain = function(params){
		return API.post(CONFIG.API_URLS.TASKS.createMultipleChain, params);
	};

	TasksService.restartTask = function(params){
		return API.post(CONFIG.API_URLS.TASKS.restart, params);
	};

	TasksService.getTasksList = function(params){
		return API.post(CONFIG.API_URLS.TASKS.records, params);
	};

	TasksService.editTask = function(params){
		return API.post(CONFIG.API_URLS.TASKS.edit, params);
	};

	return TasksService;

}]);
