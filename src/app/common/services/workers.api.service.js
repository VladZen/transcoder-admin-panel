'use strict';

angular.module('transcoder')
	.service('WorkersService',['API', 'CONFIG', function(API, CONFIG){

	var WorkersService = {};

	WorkersService.list = function(params){
		return API.post(CONFIG.API_URLS.WORKERS.getList, params);
	};

	WorkersService.getWorkerStat = function(params, workerId){
		return API.post(CONFIG.API_URLS.WORKERS.workerData + workerId, params);
	};

	WorkersService.updateWorkerStat = function(workerId){
		return API.get(CONFIG.API_URLS.WORKERS.workerData + workerId);
	};

	WorkersService.deleteWorker = function(workerId){
		return API.get(CONFIG.API_URLS.WORKERS.worker + '/' + workerId);
	};

	WorkersService.searchWorker = function(params){
		return API.post(CONFIG.API_URLS.WORKERS.workerListSearch, params);
	};

	return WorkersService;

}]);
