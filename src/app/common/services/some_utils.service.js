'use strict';

angular.module('transcoder')
	.service('SomeUtils', ['moment', function(moment){

	var SomeUtils = {};

	SomeUtils.trueUTC = function(date){
		var offset = moment().utcOffset();
		return moment(date).utc().add(offset, 'minutes').format();
	};

	return SomeUtils;

}]);
