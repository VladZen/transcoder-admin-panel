'use strict';

angular.module('transcoder')
  .directive('uiButterbar', ['$rootScope', 'CONFIG', function($rootScope, CONFIG) {
     return {
      restrict: 'AC',
      template:'<span class="bar"></span>',
      link: function(scope, el) {
        el.addClass('butterbar hide');
        $rootScope.$on(CONFIG.EVENTS.showLoading, function() {
          el.removeClass('hide').addClass('active');
        });
        $rootScope.$on(CONFIG.EVENTS.hideLoading, function() {
          el.addClass('hide').removeClass('active');
        });
      }
     };
  }]);