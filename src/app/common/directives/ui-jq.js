'use strict';

/**
 * 0.1.1
 * General-purpose jQuery wrapper. Simply pass the plugin name as the expression.
 *
 * It is possible to specify a default set of parameters for each jQuery plugin.
 * Under the jq key, namespace each plugin by that which will be passed to ui-jq.
 * Unfortunately, at this time you can only pre-define the first parameter.
 * @example { jq : { datepicker : { showOn:'click' } } }
 *
 * @param ui-jq {string} The $elm.[pluginName]() to call.
 * @param [ui-options] {mixed} Expression to be evaluated and passed as options to the function
 *     Multiple parameters can be separated by commas
 * @param [ui-refresh] {expression} Watch expression and refire plugin on changes
 *
 * @example <input ui-jq="datepicker" ui-options="{showOn:'click'},secondParameter,thirdParameter" ui-refresh="iChange">
 */
angular.module('transcoder').
  directive('uiJq', ['$timeout', function uiJqInjectingFunction($timeout) {

  return {
    restrict: 'A',
    scope: {
      uiOptions: '='
    },
    compile: function uiJqCompilingFunction() {

      return function uiJqLinkingFunction(scope, elm, attrs) {

        scope.$watch(function(){
          return scope.uiOptions;
        }, function() {
          callPlugin();
        });

        function getOptions(){
          if (scope.uiOptions) {

            var linkOptions;

            // If ui-options are passed, merge (or override) them onto global defaults and pass to the jQuery method

            linkOptions = angular.copy(scope.uiOptions);
            if (angular.isObject(linkOptions[0])) {
              linkOptions[0] = angular.extend([], linkOptions[0]);
            }

            return linkOptions;
          }


        }

        // If change compatibility is enabled, the form input's "change" event will trigger an "input" event
        if (attrs.ngModel && elm.is('select,input,textarea')) {
          elm.bind('change', function() {
            elm.trigger('input');
          });
        }

        // Call jQuery method and pass relevant options
        function callPlugin() {
          if(typeof(getOptions()) === 'object' && getOptions().length) {
            $timeout(function() {
              elm[attrs.uiJq].apply(elm, getOptions());
            }, 0, false);
          }
        }

      };
    }
  };
}]);