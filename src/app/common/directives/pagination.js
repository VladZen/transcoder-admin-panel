'use strict';
/*globals console, _ */

angular.module('transcoder')
  .directive('custompagination', ['$rootScope', 'CONFIG', function($rootScope, CONFIG) {
     return {
      restrict: 'E',
      replace: true,
      templateUrl:'/app/common/views/partials/pagination.html',
      scope: {
        options: '=',
        control: '&'
      },
      link: function(scope) {

        scope.fire = function(params){
          scope.control()(params);
        };

        scope.$watch(function(){
          return scope.options;
        }, function() {
          if(typeof(scope.options) !== 'undefined'){
            var buttonTotal = Math.ceil(scope.options.total/scope.options.limit);
            if(scope.options.skip === 0){
              scope.pagination = {
                next: {},
                prev: {},
                buttons: []
              };

              if(scope.pagination){

                for (var j = 0; j < buttonTotal; j++) {
                  if(j === 0){
                    scope.pagination.buttons.push({
                      num: j + 1,
                      skip: 0,
                      limit: CONFIG.PAGINATION_LIMIT,
                      hide: true
                    });
                  } else {
                    scope.pagination.buttons.push({
                      num: j + 1,
                      skip: CONFIG.PAGINATION_LIMIT * j,
                      limit: CONFIG.PAGINATION_LIMIT,
                      hide: true
                    });
                  }
                }
              }
            }

            scope.pagination.current_start = scope.options.skip + 1;
            scope.pagination.current_end = scope.options.skip === 0 ? CONFIG.PAGINATION_LIMIT : scope.options.skip + CONFIG.PAGINATION_LIMIT;
            scope.pagination.show = scope.options.total > scope.options.limit ? true : false;
            scope.pagination.total = scope.options.total;

            if(scope.pagination.show){
              scope.activePage = _.find(scope.pagination.buttons, {skip: scope.options.skip});

              var topBorder,bottomBorder;
              bottomBorder = scope.options.skip/CONFIG.PAGINATION_LIMIT - Math.ceil(CONFIG.PAGINATION_MAX_PAGE_LINKS/2) < 0 ? 0 : scope.options.skip/CONFIG.PAGINATION_LIMIT - Math.ceil(CONFIG.PAGINATION_MAX_PAGE_LINKS/2);
              topBorder = scope.options.skip/CONFIG.PAGINATION_LIMIT - Math.ceil(CONFIG.PAGINATION_MAX_PAGE_LINKS/2) < 0 ? scope.options.skip/CONFIG.PAGINATION_LIMIT + Math.abs(scope.options.skip/CONFIG.PAGINATION_LIMIT - Math.ceil(CONFIG.PAGINATION_MAX_PAGE_LINKS/2)) + Math.ceil(CONFIG.PAGINATION_MAX_PAGE_LINKS/2) : scope.options.skip/CONFIG.PAGINATION_LIMIT + Math.ceil(CONFIG.PAGINATION_MAX_PAGE_LINKS/2);

              for (var i = 0; i < buttonTotal; i++) {
                if(i >= bottomBorder && i < topBorder){
                  scope.pagination.buttons[i].hide = false;
                } else {
                  scope.pagination.buttons[i].hide = true;
                }
              }


              if(_.isEqual(scope.activePage, scope.pagination.buttons[scope.pagination.buttons.length - 1])){
                console.log(scope.pagination);
                scope.pagination.current_end = scope.options.total;
              }

              scope.pagination.next = {
                show: scope.activePage.skip + CONFIG.PAGINATION_LIMIT < scope.options.total ? true : false,
                skip: scope.activePage.skip + CONFIG.PAGINATION_LIMIT,
                limit: CONFIG.PAGINATION_LIMIT
              };

              scope.pagination.prev = {
                show: scope.activePage.skip - CONFIG.PAGINATION_LIMIT >= 0 ? true : false,
                skip: scope.activePage.skip - CONFIG.PAGINATION_LIMIT,
                limit: CONFIG.PAGINATION_LIMIT
              };
            }

          }
        });

      }
     };
  }]);