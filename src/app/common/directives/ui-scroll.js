'use strict';

angular.module('transcoder')
  .directive('uiScrollTo', ['$location', '$anchorScroll', function($location, $anchorScroll) {
    return {
      restrict: 'AC',
      link: function(scope, el, attr) {
        el.on('click', function() {
          $location.hash(attr.uiScrollTo);
          $anchorScroll();
        });
      }
    };
  }]);
