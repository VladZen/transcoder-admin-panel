'use strict';
/*jshint unused:false*/

angular.module('transcoder')
	.directive('uiAppear', ['$rootScope', 'CONFIG', function($rootScope, CONFIG) {
		 return {
			restrict: 'A',
			link: function(scope, el) {
				el.addClass('hide zoomIn animated');
				$rootScope.$on(CONFIG.EVENTS.showContent, function(event) {
					el.removeClass('hide');
				});
				$rootScope.$on('$stateChangeStart', function(event, toState) {
					if(toState.name.split('.').length < 3){
						el.addClass('hide');
					}
				});
			}
		 };
	}]);