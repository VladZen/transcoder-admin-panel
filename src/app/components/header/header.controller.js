'use strict';

angular.module('transcoder')
	.controller('HeaderComponentCtrl',['$scope', '$cookies', '$state', 'AuthService', function ($scope, $cookies, $state, AuthService) {
		$scope.logOut = function () {
			AuthService.logout().then(function(){
				delete $cookies['connect.sid'];
				$state.go('login');
			});
		};
	}]);
