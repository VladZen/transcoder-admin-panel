'use strict';

angular.module('transcoder')
	.controller('AsideComponentCtrl',['$scope', '$rootScope', '$location', function ($scope, $rootScope, $location) {

		$scope.$watch(function(){
			return $location.path();
		}, function() {
		  	var path = $location.path();
			$scope.isActive = function(state){
				return state === path.split('/')[1];
			};
		});

	}]);
