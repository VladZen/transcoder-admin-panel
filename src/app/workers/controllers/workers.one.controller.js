'use strict';
/*globals _ */

angular.module('transcoder')
	.controller('WorkerInsideCtrl', ['$scope', '$stateParams', 'WorkersService', '$q', 'CacheService', 'CONFIG', '$rootScope', '$timeout', 'TasksService', '$interval', 'SomeUtils', 'ModalAlert', 'moment', function($scope, $stateParams, WorkersService, $q, CacheService, CONFIG, $rootScope, $timeout, TasksService, $interval, SomeUtils, ModalAlert, moment) {

		var plotGetTrigger = false,
			plotRefreshTrigger = false,
			refreshTasksTrigger = false,
			refreshPlot, refreshTasks,
			oldPlotData;

		$scope.rangeDefault = {
			begin: moment().subtract(30, 'days').startOf('day').utc().format(),
			end: moment().format()
		};

		$scope.worker_id = $stateParams.worker_id;

		$scope.datepickerRangeBegin = {
			min_date: moment().subtract(30, 'days').utc().format()
		};

		$scope.datepickerRangeEnd = {
			max_date: moment().utc().format()
		};

		$scope.range = angular.copy($scope.rangeDefault);

		$scope.datepickerBegin = angular.copy(CONFIG.DATEPICKER);
		$scope.datepickerEnd = angular.copy(CONFIG.DATEPICKER);

		var plotGlobalOptions =	{
		  colors: ['#23b7e5', '#7266ba', '#A8DB37'],
		  series: { shadowSize: 3 },
		  xaxis:{ mode: 'time', font: { color: '#a1a7ac' }, timeformat: '%H:%M, %d/%m' },
		  yaxis:{ font: { color: '#a1a7ac' } },
		  grid: { hoverable: true, clickable: true, borderWidth: 0, color: '#dce5ec' },
		  tooltip: true,
		  tooltipOpts: { content: 'Visits of %x.1 is %y.4',  defaultTheme: false, shifts: { x: 10, y: -25 } }
		};

		var updatePlotView = function(cpu, hdd, ram) {
			$scope.plot = [
				[
			      { data: cpu, label:'CPU', points: { show: false, radius: 1}, splines: { show: true, tension: 0.4, lineWidth: 1, fill: 1} },
			      { data: hdd, label:'HDD', points: { show: false, radius: 1}, splines: { show: true, tension: 0.4, lineWidth: 1, fill: 0 } },
			      { data: ram, label:'RAM', points: { show: false, radius: 1}, splines: { show: true, tension: 0.4, lineWidth: 1, fill: 0.1 } },
			    ],
			    plotGlobalOptions
			];
		};

		$scope.seeWorkerStat = function(id, range, isFilterForm) {

			if(!plotGetTrigger){

				$rootScope.$broadcast(CONFIG.EVENTS.showLoading);

				if (refreshPlot) {
					$interval.cancel(refreshPlot);
				}

				var timeRange = {
					end: (range && range.end) ? range.end : moment().utc().format()
				};

				if(!range) {
					timeRange.begin = moment().subtract(CONFIG.DASHBOARD_PLOT_RANGE, 'minute').utc().format();
					timeRange.filter = CONFIG.DASHBOARD_PLOT_FILTER;
				} else {
					timeRange.begin = range.begin;
					if(moment(range.end).format('DD-MMM-YYYY') === moment(range.begin).format('DD-MMM-YYYY')){
						timeRange.filter = CONFIG.WORKER_VIEW_PLOT_FILTER;
					} else {
						timeRange.filter = 1000;
					}
				}

				plotGetTrigger = true;

				WorkersService.getWorkerStat(timeRange, id).then(function(res){

					if(res.data.length > 2) {
						oldPlotData = res.data;

						var sorted = _.sortBy(res.data, 'datetime'),
							cpu = [],
							hdd = [],
							ram = [];

						_.forEach(sorted, function(key) {
							var time = new Date(SomeUtils.trueUTC(key.datetime));
							cpu.push([time,key.cpu_usage]);
							hdd.push([time,key.disk_usage_percent]);
							ram.push([time,key.ram_usage_percent]);
						});

						updatePlotView(cpu, hdd, ram);
					} else {
						ModalAlert.show({
							title: 'Info',
							message: 'There is no statistics on this range.'
						});
					}



					if(!isFilterForm){
						refreshPlot = $interval(function(){

							if(!plotRefreshTrigger){
								plotRefreshTrigger = true;
								WorkersService.updateWorkerStat(id).then(function(res){

									_.forEach(res.data, function(key) {
										var time = new Date(SomeUtils.trueUTC(key.datetime));
										cpu.push([time,key.cpu_usage]);
										cpu.splice(0,1);
										hdd.push([time,key.disk_usage_percent]);
										hdd.splice(0,1);
										ram.push([time,key.ram_usage_percent]);
										ram.splice(0,1);
									});

									updatePlotView(cpu, hdd, ram);

									plotRefreshTrigger = false;
									return plotRefreshTrigger;
								});
							}

						}, CONFIG.SERVER_POLLING_PERIOD);
					}

					plotGetTrigger = false;
					return plotGetTrigger;

				});
			} else {
				$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
			}

		};

		$scope.stopTask = function(task_id, index) {
			$rootScope.$broadcast(CONFIG.EVENTS.showLoading);
			TasksService.stopTask({task_id: task_id}).then(function(){
				$scope.tasks.splice(index, 1);
				$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
			});
		};

		$scope.deleteTask = function(task_id, index) {
			$rootScope.$broadcast(CONFIG.EVENTS.showLoading);
			TasksService.deleteTask({task_id: task_id}).then(function(){
				$scope.tasks.splice(index, 1);
				$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
			});
		};



    $scope.setOneHourRange = function(){
       $scope.range.begin = moment().subtract(1, 'hours').utc().format();
       $scope.range.end = moment().utc().format();
    };

    $scope.setSixHoursRange = function(){
       $scope.range.begin = moment().subtract(6, 'hours').utc().format();
       $scope.range.end = moment().utc().format();
    };

    $scope.setTodayRange = function(){
       $scope.range.begin = moment().startOf('day').utc().format();
       $scope.range.end = moment().utc().format();
    };

    $scope.setYesterdayRange = function(){
       $scope.range.begin = moment().subtract(1, 'days').startOf('day').utc().format();
       $scope.range.end = moment().subtract(1, 'days').endOf('day').utc().format();
    };

    $scope.setThisWeekRange = function(){
       $scope.range.begin = moment().startOf('week').utc().format();
       $scope.range.end = moment().utc().format();
    };

    $scope.setThisMonthRange = function(){
       $scope.range.begin = moment().startOf('month').utc().format();
       $scope.range.end = moment().utc().format();
    };

    $scope.setLastWeekRange = function(){
       $scope.range.begin = moment().subtract(1, 'week').startOf('week').utc().format();
       $scope.range.end = moment().subtract(1, 'week').endOf('week').utc().format();
    };

    $scope.setLastMonthRange = function(){
       $scope.range.begin = moment().subtract(1, 'months').startOf('month').utc().format();
       $scope.range.end = moment().subtract(1, 'months').endOf('month').utc().format();
    };

		$scope.plot = [];

		var init = function(id) {
			$scope.seeWorkerStat(id);
			refreshTasks = $interval(function(){

				if(!refreshTasksTrigger){
					refreshTasksTrigger = true;
					TasksService.getCurrent(id).then(function(res){
						$scope.tasks = res.data;
						refreshTasksTrigger = false;
						$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
						$rootScope.$broadcast(CONFIG.EVENTS.showContent);
					});
				}

			}, CONFIG.SERVER_POLLING_PERIOD);
		};

		init($scope.worker_id);

		$scope.$on('$destroy', function() {
          $interval.cancel(refreshPlot);
          $interval.cancel(refreshTasks);
        });

	}]);