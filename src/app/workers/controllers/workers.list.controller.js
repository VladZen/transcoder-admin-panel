'use strict';

angular.module('transcoder')
	.controller('WorkerslistCtrl', ['$scope', 'WorkersService', '$q', 'CacheService', 'CONFIG', '$rootScope', '$timeout', function($scope, WorkersService, $q, CacheService, CONFIG, $rootScope, $timeout) {

		var workersRequestParams = {
			skip: 0,
			limit: 10
		};

		var lastParams, oldQuery;

		var requestProcesser = function(res, params, controller, cacheID){
			if(cacheID){
				CacheService.put(cacheID, res);
			}
			$scope.workers = res.data.workers;
			$scope.pagination = {
				skip: params.skip,
				limit: params.limit,
				total: res.data.total,
				controller: controller
			};
			lastParams = params;
			$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
		};

		$scope.getWorkers = function(params) {
			var cacheID, data, deferred;
			cacheID = 'WorkersList' + JSON.stringify(params);
			deferred = $q.defer();
			data = CacheService.get(cacheID);
			$scope.resultSearch = false;

			$rootScope.$broadcast(CONFIG.EVENTS.showLoading);

			if (data != null) {
				$timeout(function(){
					requestProcesser(data, params, $scope.getWorkers);
					$rootScope.$broadcast(CONFIG.EVENTS.showContent);
				}, 100);
			} else {
				WorkersService.list(params).then(function(res){
					requestProcesser(res, params, $scope.getWorkers, cacheID);
					$rootScope.$broadcast(CONFIG.EVENTS.showContent);
				});
			}
		};

		$scope.searchWorkers = function(params){
			var cacheID, data, deferred;
			cacheID = 'SearchResults' + JSON.stringify(params);
			deferred = $q.defer();
			data = CacheService.get(cacheID);
			if(oldQuery && oldQuery !== params.query){
				oldQuery = params.query;
			} else {
				params = {
					skip: 0,
					limit: 10,
					query: params.query
				};
			}

			if(!params.query){
				params.query = oldQuery;
			}

			$rootScope.$broadcast(CONFIG.EVENTS.showLoading);

			if (data != null) {
				$timeout(function(){
					requestProcesser(data, params, $scope.searchWorkers);
					$scope.resultSearch = true;

				}, 100);
			} else {
				WorkersService.searchWorker(params).then(function(res){
					requestProcesser(res, params, $scope.searchWorkers, cacheID);
					$scope.resultSearch = true;
				});
			}

		};

		$scope.deleteWorker = function (task_id) {
			$rootScope.$broadcast(CONFIG.EVENTS.showLoading);
			WorkersService.deleteWorker(task_id).then(function(){
				CacheService.removeAll();
				if(lastParams.query) {
					$scope.searchWorkers(lastParams);
				} else {
					$scope.getWorkers(lastParams);
				}
				$rootScope.$broadcast(CONFIG.EVENTS.hideLoading);
			});
		};


		var init = function() {
			$scope.getWorkers(workersRequestParams);
		};

		init();

	}]);