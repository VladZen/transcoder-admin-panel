# README #

### Set up ###

NEVER use "bower install" as there are manual changes in bower_components!

To set the app on your local machine you need to follow these steps:

1. First of all you need to install nodeJS. [Here](https://nodejs.org/download/) you can download the installer. After the installation open your terminal and enter this command to check out the version of your node:
>node -v

2. Secondly you need to install gulpJS. [Here](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md) is the detailed manual how to install gulp. Basically you need to enter the following command in your terminal:
>sudo npm install --global gulp

2.1 Clone frontend repository from bitbucket: git clone https://Pietro777@bitbucket.org/Pietro777/frontend.git

3. After all preparings and installations you should enter your project folder in terminal:
>cd [absolute path to the project on your local] ('cd frontend' from directory where is repository was cloned.)

4. Then you should install all node modules using command:
>sudo npm install

5. To start your work with the production minified version of the app enter this command:
>gulp serve:dist

6. To start your work with the development version of the app enter this command:
>gulp serve

7. To build the app for the deploy enter this command:
>gulp
or
>gulp build
it will build the version in /dist folder
